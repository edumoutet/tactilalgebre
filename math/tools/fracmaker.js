/**Utilitaires **/

var factor=[2,3,4,5,6,7,8,9];

var litteral=['a','b','c','x','y','z','n'];

var prems = [2, 3, 5, 7];

var times = String.fromCharCode(215);
var minus = String.fromCharCode(0x2212);

/*Les surdou�s du js ont consid�r� que cha�ne vide = nombre*/
function acceptNumber(x) {
    return x != '' && (!isNaN(x) || x.match(/[xnabc]/))
}

function randomSimpleFactor(){
  return factor[randomInt(factor.length)];
}

function randomInt(maxi){
   var rand = Math.random()*maxi;
   return Math.floor(rand);
}

function factorCollec(n){
  var result=[];
  for(var k=0;k<n-1;++k){
    result.push(randomSimpleFactor());
  }
  result.push(1+randomInt(20));
  return result;
}

/**Fraction al�atoire**/
function fracMaker(nbfactor,nbfrac){	
   var nums=factorCollec(nbfactor);
   var dens=factorCollec(nbfactor);
   var numFin=[],denFin=[];
   for(var nf=0;nf<nbfrac;++nf) {numFin.push(1); denFin.push(1);}
   for(var nf=0; nf<nbfactor; ++nf){
   	  var ndfrac = randomInt(nbfrac);
   	  numFin[ndfrac]*=nums[nf];
   	  denFin[ndfrac]*=dens[nf];
   }
   return [numFin,denFin];
}

/**Inusit�**/
function raffiner(numden){
	var num=numden[0],den=numden[1];
	var numraf=[], denraf=[];
	for(var nd=0;nd<num.length;++nd){
		var onlynum = (den[nd]==1);
		var exprNum = num[nd];  //tableau de paires (nbre, exposant)
		numraf.push({val: exprNum, islone: onlynum});
		numraf.push({val:'x',islone:true});
		if(!onlynum) denraf.push({val: den[nd]});
	}
	numraf.pop();  //Enlever op�rateur en trop
	return [numraf,denraf];
}

/**Calcul brutal**/
function expon(a,b){
  var result=1;
  while(b--) result*=a;
  return result;
}

function pgcd(a,b){
   var r=a%b;
   if(r==0) return b;
   else if(r>1) return pgcd(b,r);
   return 1;
}

/**Gestion des decompos en nombres premiers=>version normalis�e de exprod**/
function decompoPremier(val){
	var resu={}
	if(isNaN(val)) {resu[val]=1; return resu;}  //Litt�ral
	//val=decimal(val);  //G�re nbres d�cimaux
	var expos={};
	prems.forEach(function(p){
		var exp=0;
		while(!(val%p)){++exp;val=val/p;}  //ATT priorit�s!
		if(exp) expos[p]=exp;
	});
	if (val < 0) { expos["-1"] = 1; val = -val; }
	if(val>1) expos[val]=1;  //TODO: g�rer val<0
	return expos;
}

/**function decimal_(nmber){
	var deci=nmber.toString().match(/[.](.+)/);  //Nombre � virgule
	if(deci==null) return nmber;
	return [[deci[1],1],[10,-deci[1].length]]
}**/

function decimal(nmber){
	var duo=nmber.toString().split('.');
	if(duo.length<2) return nmber;
	var intpart=duo[0]+duo[1];  //Concatene les deux parties
	var ndec=duo[1].length;
	return [[intpart,1],[10,-ndec]];
}

function expodec(ndec){
	var resu=1;
	while(ndec--)resu*=10;
	return resu; 
}

/**PARSING**/
/**a^n*b^p=>tableau de paires (operande,exposant): version non normalis�e**/
function parseExpoProd(expr,errors){
	if(!errors) errors=[];  //Mode silencieux: errors ne sort pas de cette fonction
	if(isOpe(expr)) {errors.push("Seulement op�rateur"); return false;}
	var tab = expr.split(times); 
	return parseTab(tab, errors);  
}

function textToProd(txt){
	return (""+txt).split(times);
}

/*Op�ration entre deux expoProd*/
function parseOperation(nums,dens,method){
	var exponum=toPrems(parseTab(nums,[]));
	var expoden=toPrems(parseTab(dens,[]));
	return method(exponum, expoden);
}

function parseOperationFromText(numtext,dentext,ope){
	return parseOperation(textToProd(numtext),textToProd(dentext),ope=='/'?divide:multiply);
}

function supify(duo){
	var nmb=duo[0];
    var expo=false;
    if(duo.length==2) expo=duo[1];
    if(expo==1) expo=false;
	return expo? nmb+"<sup>"+expo+"</sup>":nmb;
}

function spanify(src){
	return "<span>"+src+"</span>";
}

/*expo NON normalis�e**/
function expoProdToHtml(expo){
	var htms=[];
	expo.forEach(function(item){
		htms.push(spanify(supify(item)));
	});
	return htms.join('x');
}

/**Rend html d'une expression comme x^3**/
function expoToHtml(txt){
	var duo = (""+txt).split('^');
	if(duo.length==2) return duo[0]+"<sup>"+duo[1]+"</sup>";
	else return duo[0];
}

function htmlToExpo(src){
}

/**NON normalis�
retourne tableau(nbre,exposant)**/
function parseTab(tab, errors){
	var result=[];  //Oblig� � cause de clefs doubles possibles
	for(var nd=0;nd<tab.length;++nd) {
		  ncurr = ""+tab[nd];
		  var texp=ncurr.split('^');   //Attend x^n
		  var nb = texp[0], nb1;
		  nb = nb.replace(minus, '-');  //cf numericBoard
		  if (!acceptNumber(nb)) {
		      console.log(err = "parse error:bad number:" + nb); errors.push(err);
		      return false;
		  }
		  if (texp.length == 2) {
		      nb1 = texp[1].replace(minus, '-');
		      if (isNaN(nb1)) {
		          console.log(err = "parse error:bad number:" + nb1); errors.push(err);
		          return false;
		      }
			  result.push([nb,nb1]);	 
		  }	 
		  else result.push([nb,1]);
	  }
	  return result;
}

/**Pour mettre une exprod sous forme d'exprod de nbres premiers ie normalis�e ie objet{clef=nbre,value=exposant}**/
function toPrems(exprs){
	var result={}
	console.log("toPrems");
	console.log(exprs);
	exprs.forEach(function(item){
		var nmber=decimal(item[0]);
		var n1={};
		if(!isNaN(nmber))  //Nmbre
		    n1=decompoPremier(nmber);
		else if(Array.isArray(nmber))//D�cimal 
			n1=toPrems(nmber);
		else n1[nmber]=1;   //litt�ral 
		exponent(n1,item[1]);
		multiply(result,n1);
	});
	return result;
}

/*helper pour la suite*/
function setSign(expr) {
    var s = expr[-1];
    if (!s) return;
    s = s % 2;
    expr[-1] = s<0?-s:s;
}

/**Pour expoprod normalis�e
 * return expoprod
 **/
function exponent(expoprod,n){
	if(isNaN(n)) return;
	for(var key in expoprod)
	    expoprod[key] *= n;
	setSign(expoprod);
}

/**return expr1*/
function multiply(expr1,expr2){
	for(var key in expr2)
	  if(key in expr1) expr1[key]+=Math.floor(expr2[key]);
	  else expr1[key] = expr2[key];
	setSign(expr1);
	return expr1;
}

/**return expr1*/
function divide(expr1,expr2){
	for(var key in expr2)
	  if(key in expr1) expr1[key]-=Math.floor(expr2[key]);
	  else expr1[key] = -expr2[key];
	setSign(expr1);
	return expr1;
}

var opeSelec={'/':divide,'*':multiply,'^':exponent}

/**Pour forme normalis�e**/
function toHtml(exprod){
	var htmlList=[];
	for(var key in exprod){
	  var expo = exprod[key];	
	  htmlList.push("<span>"+key + (expo!=1?"<sup>"+expo+"</sup>":"") +"</span>");
  	}
	return htmlList.join('x');
}

/**Schema:texte=>exprod=>normalisation=>comparaison**/
function compareExpoProd(ex1,ex2)
{
	var result={correct: false, ex1: ex1, ex2: ex2}
	for(var key in ex1){
		if(ex1[key]!=(0|ex2[key])) 
		return result;}//Incorrect
	result.correct=true;
	return result;
}

/**nums,dens: r�sultat; fracs donne le calcul �l�ve, on compare les deux**/
function checkWork(){
	var nums = vm.num();
	var dens = vm.den();
	var myExpr = parseFrac(nums,dens);  //R�sultat compact
	
	var fracs=htmlToFraction();	
	var numstud=[];
	var denstud=[];
	fracs.forEach(function(fr){
		var num=fr.upAsInt;
		if(num && !(fr.isOpe)) numstud.push(num);
		var den=fr.downAsInt;
		if(den) denstud.push(den);
	});
	/*console.log("checkwork:");
	console.log(numstud);
	console.log(denstud);*/
	var studExpr = parseFrac(numstud,denstud);
	return compareExpoProd(myExpr, studExpr);
}

/**TEST**/
function testDecompo(txt,errors){
	var exprod = parseExpoProd(txt,errors);
	//console.log(exprod);
	if(!exprod) return false;
	var exnorm = toPrems(exprod);
	//console.log(exnorm);
	return exnorm;
}

function normalizeExprod(){
	var errors=[];
	var txt = $("#test").val();
	var norm = testDecompo(txt,errors);
	var resu = $("#resutest");
	if(norm) resu.html(toHtml(norm));
	else errors.forEach(function(err){resu.append(err+'<br/>')});
}

var SUP=/[^](.*)/;
  var SUB=/[_](.*)/;
  /**todo: mettre des exposants...**/
  function replaceSymb(expr,symb){   
	 var r1 = symb.exec(expr);
	 //console.log(r1);
	 if(r1)
	 return expr.replace(r1[0],"<sup>"+r1[1]+"</sup>");
	 else return texte;
  }
 
  /**Reconnaitre une bonne d�composition-d�pr�ci�**/
function eval_produit(expr,val){
  var tab = expr.split(times);
  var nb0=tab[0];
  for(var nd=1;nd<tab.length;++nd) {
	  nb0*=tab[nd]; if(nb0 > 65535) return false;
	  }
  return (nb0==val);
} 
  //var r = expr.replace(/_(.*)/, function(item){return "<sub>"+item+"</sub>";});


