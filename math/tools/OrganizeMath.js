﻿/*****Primitives graphiques******/
    var grid = 16;
    var DX = 64, DY = 48;

    var RAY = DX / 2;
    
    var operands = [];  //Tableau de travail poour decoder le script
    var operators = [];
    
    var bgcolor = { "number": "#eee", "operator": "green", "resultPartial": "#88a", "result": "#dd8" }

/*Objet principal (binding knockout)*/
    var slate={
	    operands: ko.observableArray(),
	    operators: ko.observableArray(),
	    mathEntity: ko.observableArray(),
	    add: function(ent){slate.mathEntity.push(ent);},
	    setbg: function(ent){
		    return bgcolor[ent.type];
		    }
    }

    function centre(obj)
    {
	    return {x: obj.x+DX/2, y: obj.y+DY}
    }
    
    function clear(){
	    var canvas = document.getElementById("c");
		if(canvas){
	    var ctx = canvas.getContext("2d");
	    ctx.clearRect(0,0,canvas.width,canvas.height);
		}
    }
    
    function drawLine(obj1,obj2) {
	    var p1=centre(obj1);
	    var p2=centre(obj2);
	    var canvas = document.getElementById("c");
	    var ctx = canvas.getContext("2d");
	    ctx.beginPath();
	    ctx.moveTo(p1.x,p1.y);
	    ctx.lineTo(p2.x,p2.y);
	    ctx.stroke();
	    ctx.closePath();
    }
   
  
    //add objects
slate.addPlaceNumber=function(x,y)
{
    var result = {x: x, y:y, type: "number"};  //nb:false
    slate.add(result);
    operands.push(result);
    return result;
}

/***Construit emplacement pour l'op?rateur et aussi pour le r?sultat***/
slate.addPlaceOpe = function() {
    var nb1 = operands.pop();
    var nb2 = operands.pop();    //Il en faut 2 au moins...
    //var top = (nb1.y + nb2.y) / 2 + DY;
    var top = Math.max(nb1.y, nb2.y) + DY;
    var left = (nb1.x + nb2.x) / 2;
    var result = { x: left, y: top, type: "operator", nb1: nb1, nb2: nb2, resu: false };  // operator: false,
      
    //drawLine(nb1.x, nb1.y, left, top);
    //drawLine(nb2.x, nb2.y, left, top);  //Relie les op?randes ? l'op?rateur!
    slate.add(result);
    return result;
}

/***Emplacement éditable***/
slate.addIText=function(operator,value)
{
    var x = operator.x;
    var y = operator.y + DY;
    var txt = {x: x, y: y, type: "resultPartial"};  //text:value
    slate.add(txt);
    //operator.resu = txt;
    operands.push(txt);
}

slate.addResu=function(operator,resu) {
   var x = operator.x;
   var y = operator.y + DY;
   slate.add({x:x, y:y, resu:resu, type:"result"});
}

slate.addMovableNumber=function(value)
{
	slate.operands.push(value);   
}

slate.addMovableOperator=function(value)
{
	slate.operators.push(value);   
}

function getObjectsOfType(type) {
    var result = new Array();
    slate.mathEntity().forEach(function (obj) { if (obj.type == type) result.push(obj); });
    return result;
}

function relyObjs(){
	var operators = getObjectsOfType("operator");
	operators.forEach(function(obj){
		drawLine(obj,obj.nb1);
		drawLine(obj,obj.nb2);
	});
}

/***Transforme un script(RPN: notation polonaise invers?e) en s?quence visuelle ? remplir
remplit tableau observable slate.mathEntity
Exemple: [14 15 + 7 x] ***/

function decodeScript(scr,resu) {
	 	
  slate.mathEntity.removeAll();
  slate.operands.removeAll();
  slate.operators.removeAll();
  	
  var opes = "_+x:-";
  var X=0, Y=DY/2;
  var NBITEM = scr.length;
  var isNumber=false;
  
  for(nd=0; nd < NBITEM; ++nd)
  {
	var th = scr[nd];
	if(isNaN(th)) {  //Op?rateur
	   Y+=DY;
	   X-=DX;
	   var operator=slate.addPlaceOpe();
	   slate.addMovableOperator(th);
	   Y+=DY;
	   if(nd < NBITEM-1) slate.addIText(operator,"000");  //Emplacement r?ponse
	   else slate.addResu(operator,resu);
	   isNumber=false;
	}
	else {   //Nombre, opérande	  
		//if(!isNumber) X+=DX/2;  //Deux nombres au moins 
	    slate.addMovableNumber(th);
	    slate.addPlaceNumber(X, Y);
		X += DX * 2;
		isNumber=true;
	}
  }
  
}

/**Emploie la RPN (reverse polish notation)**/
function decodeScriptFromString(value)
{
	var tab = value.split(" ");
	console.log(tab);
	var resu = tab.pop();
	
	decodeScript(tab,resu);
}

/*Gérer le drag and drop*/
function linkAll()
{
	if(!document.getElementById("c")) $("#slate").append("<canvas id='c' width='600' height='600'></canvas>");
	$("#operands p").draggable({revert:true});
	$("#operators p").draggable({ revert: true });
	$("#numericBoard").css("display", "none");

	var stopProp = false;
    
    function associate(from,to){
        $(to).droppable({
            accept: from, drop: function (event, ui) {
                stopProp = true;
                console.log("associate drop");
	           $(this).append(ui.draggable.detach()); 
	           ui.draggable.css("top","0px");
			   ui.draggable.css("left","0px");}});
    }
    
    associate(".mov_number",".number");
    
    associate(".mov_operator",".operator");

    associate(".mov_number", "#operands");

    associate(".mov_operator", "#operators");

    $("#slate").droppable({
        accept: ".mov_number", drop: function (event, ui) {
            if (stopProp) { stopProp = false; return; }
            console.log("slate drop");
            $("#operands").append(ui.draggable.detach());
            ui.draggable.css("top", "0px");
            ui.draggable.css("left", "0px");
        }
    });
    
    $(".resultPartial").click(function(){
        //var result = prompt("Editer:", $(this).find("p").html());
        //$(this).html("<p class='math_entity'>"+result+"</p>");
        $("#menu").hide();
        slate.nb.beginEdit();
        slate.nb.target($(this));
    });

    relyObjs();
}

/****Evaluation****/
var operation = function () {
    this.ope1 = false;
    this.ope2 = false;
    this.operand = false;
    this.calculer = function () {
        if (!this.ope1 || !this.ope2) return false;
        var n1 = (this.ope1.calculer ? this.ope1.calculer() : this.ope1);
        var n2 = (this.ope1.calculer ? this.ope1.calculer() : this.ope1);
        return operer(n1, n2, this.operand);
    }
};

function operer(n1, n2, operand)
{ return 1; }  //todo!!


function corrige() {
    assoMobile();
    assoOpe();
    var opes = getObjectsOfType('operator');
    for(nd in opes)
        {
        var ope = opes[nd];
        if (!verifOpe(ope)) return "Erreur:"+_toString(ope); 
        }
    return true;
}

function _toString(ope) {
    with (ope) {
        if (!nb1 || !nb2) return false;
        var n1 = (nb1.nb ? nb1.nb : nb1.getText());  //Selon qu'on ait IText(getText()) ou rectangle(champs nb)
        var n2 = (nb2.nb ? nb2.nb : nb2.getText());
        return (n1 + operator + n2 + "==" + resu.getText());
    }
}

function printResu() {
    var result = "";
    getObjectsOfType("operator").forEach(function (ope) { result += "|" + _toString(ope); });
    return result;
}

function verifOpe(ope) {
    return eval(_toString(ope));
}

/*Gestion de script créant un arbre de calcul*/
//decodeScript([9, 8, '*', 5, 3, '+', '*'], 576);
function expressionParser(src)
{
	var tab = src.split(/[()]/);
	return tab;
	var len = src.length;
	for(var nd=0; nd<len; ++nd){
		var ch = src.charAt(nd);
	}
}

/**Jsep management**/
/**On va lin?ariser avant...**/
function lineariseJsep(expr)
{
	var result = [];
	function add(expr2){
	  if("BinaryExpression"==expr2.type) result = result.concat(lineariseJsep(expr2));
	  else result.push(expr2.value);
    }
    add(expr.left);
    add(expr.right);
	result.push(expr.operator);
	return result;
}

function jsepBinary(tree){
   if(val=tree.raw) return val; 
   var x=tree.left;
   var operator=tree.operator;
   var y=tree.right;
   return "=&gt;<ul><li>"+jsepBinary(x)+"</li><li>"+operator+"</li><li>"+jsepBinary(y)+"</li></ul>";
}

function displayJsepAsList(src){
	parse_tree=jsep(src);
	$("#container").append(jsepBinary(parse_tree));
}

/*Gestion de script créant un arbre de calcul*/
var parse_tree;
function decodeScriptViaJsep(src)
{
	parse_tree = jsep(src);
	console.log(parse_tree);
	var tab = lineariseJsep(parse_tree);
	console.log(tab);
	decodeScript(tab,0);
	//$("body").append(tableJsep(parse_tree));
}

function showMathJsep(src){
	parse_tree = jsep(src);
	console.log("showMathJsep");
	console.log(parse_tree);
	return tableJsep(parse_tree);
}

/*return {wid,hei}-faire un arbre de bo?te - d'abord wid et hei */
function positionJsep(tree){
	var x = tree.left;
	if(x.raw) lbox= {wid: 32, hei: 32}; else lbox = positionJsep(x);
	var y = tree.right;
	if(y.raw) rbox={wid:32, hei:32}; else rbox = positionJsep(y);
	if(tree.operator != '/') result = {wid: lbox.wid + 16 + rbox.wid, hei: Math.max(lbox.hei, rbox.hei)};  //En largeur
	else result = {wid: Math.max(lbox.wid,rbox.wid), hei: lbox.hei+8+rbox.hei} //En fraction
	tree.box = result;
	return result;
}

var linearise={'_':true,'^':true}

/**Facultatif lin, parentOp
lin: certaines op?rations n?cessitent un op?rande simple(on force) - pour ^ et _ 
parentOp: on mettra des parenth?ses si n?cessaires.(priorit? parentOp>this operator)
**/
function tableJsep(tree,lin,parentOp){
	if(tree.callee) { 
		var name=tree.callee.name;
		var body = tableJsep(tree.arguments[0],true,false);       //todo: éviter la linéarisation (mettre name et body ds table)
		if(name=='rac') return String.fromCharCode(8730) + body;  //Racine carrée
		else if(name=='abs') return '|'+body+'|';           //Valeur absolue
	    return name+'('+body+')';
	   }
	if(tree.raw) return tree.raw; else if(tree.name) return tree.name;  //Constante ou litteral(ex: x)
	
	var left,right,x;    //Expression unaire ou binaire
	var y=tree.argument;   //Opérateur avec opérande seul
	var operator=tree.operator;
	if(y){
		left='';
	}else{
	    x = tree.left;     //Opération avec deux opérandes
	    left = tableJsep(x,false,operator);
	    y = tree.right;
	    if(!y.raw && operator=='*') operator='';   //Opérateur * sous-entendu
    }    
	right = tableJsep(y,linearise[tree.operator],tree.operator);   //On oblige de linéariser après _ et ^
	
	console.log("tableJsep:"+left+operator+right);
	
	if(lin) return left+operator+right;
	
	if(operator=='^'){ 
	    if(x && x.callee) return "<div style='display:inline-block'>"+insertExpo(left,right)+"</div>";  //L'exposant est après f et non f(x)
	    //return "<div style='display:inline-block'>"+left+"<sup>"+right+"</sup></div>";
	    if(x.operator||x.argument) left='('+left+')';  //TEST
	    console.log(r=left+"<sup>"+right+"</sup>");
	    return r;
		}
		//return "<table><tbody><tr><td></td><td><small>"+right+"</small></td></tr><tr><td>"+left+"</td><td></td></tr></tbody></table>"; option
	else if(operator=='_') 
	    return "<div style='display:inline-block'>"+left+"<sub>"+right+"</sub></div>";
	    
	else if(operator=='/') 
		return "<table><tr><td style='border-bottom: 1px solid black'>"+left+"</td></tr><tr><td>"+right+"</td></tr></table>";
		
	else {
		var openExp='',closeExp='';
		if(jsep.givePriority(parentOp,tree.operator)) {openExp='(',closeExp=')';}  //Besoin des parenth?ses pour donner la priorit? au plus faible op?rateur
		
		return "<table><tr><td rowspan='2'>"+openExp+"<td>"+left+"</td><td>"+operator+"</td><td>"+right+"</td><td rowspan='2'>"+closeExp+"</tr></table>";
		//return openExp+left+operator+right+closeExp;
	}
}

function insertExpo(data,exposant){
    return data.replace('(',"<sup>"+exposant+"</sup>(");
}

