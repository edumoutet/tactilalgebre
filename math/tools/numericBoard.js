//var entry="currentEdit";
var debug = true;  //Forcer le numeric board
function detectMobile() {
        if (navigator.userAgent.match(/Android/i)
           || navigator.userAgent.match(/webOS/i)
           || navigator.userAgent.match(/iPhone/i)
           || navigator.userAgent.match(/iPad/i)
           || navigator.userAgent.match(/iPod/i)
           || navigator.userAgent.match(/BlackBerry/i)
           || navigator.userAgent.match(/Windows Phone/i)
           ) {
            return true;
        }
        else {
            return debug;
        }
}

function numericBoard(){
var tamil=0xbf9;
var eraseLeft=String.fromCharCode(0x232b);
var minus=String.fromCharCode(0x2212);
var times=String.fromCharCode(215);
var sqrt = String.fromCharCode(0x221A);

/*manage sup (exposant)*/
var supHex=[0xb0,0xb9,0xb2,0xb3,0x2074,0x2075,0x2076,0x2077,0x2078,0x2079];  //Exposants 0..9
var supLiteralHex=[0x2071,0x207d,0x207e,0x207f,739]; //Litt�raux: i()bx
var sups=[];

function convertChar(hex) { sups.push(String.fromCharCode(hex)); }

supHex.forEach(convertChar);

var opeHex={'+':0x207a, '-':0x207b, 'n':0x207f}

sups['n']=String.fromCharCode(0x207f);
sups['x']=String.fromCharCode(739);
sups['+']=String.fromCharCode(0x207a);
sups[minus]=String.fromCharCode(0x207b);
sups['(']=String.fromCharCode(0x207d);
sups[')'] = String.fromCharCode(0x207e);

    /*Le calcul formel n'emploie pas les jolis caract�res
    var rminus = new RegExp(minus, 'g');
    var rtimes = new RegExp(times, 'g');
    function translateOpe(data) { return data.replace(rminus, '-').replace(rtimes,'x');}*/

var self=
    {
        name: "#numericBoard",

    nums: [1, 2, 3, 4, 5, 6, 7, 8, 9, 0],

    entry: ko.observable("currentEdit"),  //NB: client+entry utilisent ko, pas target

    beginEdit: function (field, callback) {
        if (field) self.entry(field);
        $("#numericBoard").show();
        if (callback) {
            self.endEdit = function () {
                callback(self.client[self.entry()]());
                self.cancelEdit();
            }
        }
    },
	
	opes:['^',times,'+',minus,'/',sqrt,'.','(',')','='],
	
	litteral:['x','y','z','a','b','c','n'],
	
	supset: ko.observable(false),

	currentEdit: ko.observable(''),

	errorMessage: ko.observable(),

    niceChars:false,
	 
	getEdition: function(){return self.client[self.entry()]();},  //Modification sans utiliser de callback
	 
	setEdition: function (data) { self.client[self.entry()](data) },
	 
	addNumber:function(data){
		 var dest=data;
		 if(self.supset()) dest=sups[data];
		 if(dest!=undefined) 
		   self.setEdition(""+self.getEdition()+dest);  //On emploie des cha�nes et non des nombres!
	},

	addOpe:function(data){
	    switch (data) {
	        case '^': self.supset(!self.supset()); break; //Toggle exposant
	        case minus: if (!self.niceChars) self.addNumber('-'); break;
	        case times: if (!self.niceChars) self.addNumber('x'); break;
	        default: self.addNumber(data);
	    }
	},

	backEdit:function(){
		var back = ""+self.getEdition();
		self.setEdition(back.substr(0,back.length-1));
	},

	cancelEdit: function () { $("#numericBoard").hide(); $("#menu").show(); self.currentEdit(""); },

        /*NEW:*/
        //Donn� par programme appelant! utilise le callback qui suit pour modifier html de target
	 target: ko.observable(),

        //Utilise target; red�finir cette m�thode si on ne souhaite pas utiliser target
     endEdit: function () { if (elem = self.target()) elem.html(self.currentEdit()); self.cancelEdit();}  
    }
    self.client = self;
    self.setErrorMessage = function (message) {
        self.errorMessage(message);
        $("#errorMessage").show({ complete: function () { $("#errorMessage").fadeOut({ duration: 3000 }); } });
    }
    /*ko.computed(function () {
        var data = self.client[self.entry()]; data(translateOpe(data));
    });*/
 	return self;
	}