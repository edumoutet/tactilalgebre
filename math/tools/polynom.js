var RegParse=/(x|[()]|[+-]|[.0-9]+)/g;

/*expre=5x3+3x2-2x+7
result=5*x^2+3*x^2-2*x+7 
BUT: rendre disponible pour jsep*/
function verbosePolynom(expre){
	var test1=expre.replace(/[0-9]+x/g, intercaleProd);
	var test2=test1.replace(/x[0-9]+/g,intercaleExpo);
	function intercaleProd(item){
		return item.replace('x','*x');
	}
	function intercaleExpo(item){
		return item.replace('x','x^');
	}
}

/*Inusit�*/
function extraitCoefs(expre){
	var expos=[];
	var test1=expre.replace(/-?[.0-9]+x/g, extrExpo);
	function extrExpo(item){
		expos.push(item.replace('x',''));
	}
}

/**Vrai depart! normalis� ou pas
{x,sign,nmber,degree} vs {nmber,degree}***/
function displayPolynom(poly){
	var htm="";
	
	poly.forEach(function(monom){
		var nmb=monom.nmber;
		if(monom.sign)
		   htm += monom.sign;
		else if(nmb>0)  
		   {htm+='+';
	       }
	    if(nmb==1 && (0==monom.degree)) htm+='1';
	    else{
		  if(nmb && (nmb!=1 || !monom.degree)) htm += nmb;
		  if(monom.x||monom.degree>=1) htm+='x';  //OU pour les deux cas
	    }
	    if(monom.degree>1) htm+='<sup>'+monom.degree+'</sup>'; 
	    if(monom.children){
		  monom.children.forEach(function(child){
			htm+='('+displayPolynom(child)+')';
			if(child.exponent) htm+=supify(child.exponent);
			});
	    }
		if(monom.exponent) htm+=supify(monom.exponent);
	});
	
	//console.log("html=");
	//console.log(htm);
	return htm;
}

function supify(expo){
	return '<sup>'+expo+'</sup>';
}

/**function displayNormaPolynom(poly){
	var monoms=[]; 
	for(var key in poly){  //On reconstruit un poly non normalis�
		monoms.push({nmber:poly[key],degree:key});
	}
	return displayPolynom(monoms);
}**/

/*monom=produit monom par polyn�mes comme 7x(3-x)(2+x3)**/
function parseAtoms(atoms,pred){
	var poly=[];
	var monom=false;
	var na=0;
	var lastChild=false;
	while(na<atoms.length){
		//if(!pred) {console.log("parseAtoms:avance:"); console.log(na+'/'+atoms[na]); }
		  var atom=atoms[na];
			if(/[+-]/.exec(atom)) {  //S�parateur de mon�mes
				if(monom) poly.push(monom);
				monom={sign:atom,x:false};  //Nouveau mon�me
			}
			else if (!isNaN(atom)) {//Nmber
				if(!monom) monom={x:false}
				if(monom.lastChild) monom.lastChild.exponent=atom;  //Genre (x-a)^n
				else if(monom.x) monom.degree=atom;
				else monom.nmber=Math.floor(atom);
			}
			else if(atom=='x') {
				if(!monom) monom={}
				if(!monom.x){�
				   monom.x=true;
				   monom.degree=1;
			   }else na+=parseAtoms(atoms.slice(na,atoms.length),monom);  //Enfant d�marre par x
			}
			else if(atom==')')
			break;
			else if(atom=='('){
				//console.log("recursif");
				if(!monom) monom={};
				na+=parseAtoms(atoms.slice(na+1,atoms.length),monom);  //R�cursif..
				//else monom=parseAtoms(atoms.slice(na+1,atoms.length));
			}
			na++;
		}
		if(monom) poly.push(monom);
		if(pred) {//console.log("pred:");console.log(poly); 
			addChild(pred,poly);return na+1;}
		return poly;
}

function addChild(poly,child){
	if(!poly.children) poly.children=[];
	poly.children.push(child);
	poly.lastChild=child;
}

function parseBrut(expre){
	var atoms=expre.match(RegParse);
	var monoms = parseAtoms(atoms);
	console.log("Avant normalisation:");
	console.log(monoms);
	return monoms;
}

function displayBrut(expre){
	var monoms=parseBrut(expre);
	console.log("displayBrut:");
	console.log(monoms);
	return displayPolynom(monoms);
}

/**Expression du type 3x2-7x3+8x-41=>[monom={sign,nmber,x,degree}]**/
function parsePolynom(expre,checkProduct){
	var monoms=parseBrut(expre);
	//G�rer les mon�mes-produit
	var polys=[];
	var dev;
    monoms.forEach(function(m){
	    dev=normaMonom(m);
	    //console.log(dev);
	    polys=polys.concat(dev);
    });	
    console.log("parsePolynom=>Dev brut");
    console.log(polys);
    var final=normaPoly(polys);
    final.size=monoms.length;
    console.log("parsePolynom: normalis�");
    console.log(final);
	if(!checkProduct) return final;
	return {poly: final, size: final.size}
}

/*return forme normalis�e ie {nmber, degree}**/
function multiplyMonom(m1,m2){
	m1 = normaMonom(m1);
	m2 = normaMonom(m2);
	var resu={normalized: true}
	resu.nmber=m1.nmber*m2.nmber;
	if(resu.nmber) resu.degree=Math.floor(m1.degree)+Math.floor(m2.degree);
	else resu.degree=false;  //Mon�me nul
	return resu;
}

/**Calcule un d�veloppement en fait, le mon�me brut peut �tre un produit
Retourne un polyn�me ou bien un mon�me!**/
function normaMonom(monom){
	//console.log("begin normaMonom:");
	    if(monom.normalized) {
		    console.log(monom); 
		    console.log("end monom"); 
		    return monom;
		    }
	    var resu={nmber: monom.nmber, normalized:true}
		if(typeof(monom.nmber)=='undefined') resu.nmber=1;  //x ou xn ou 0
		
		if(!monom.x) resu.degree=0;     //Constante
		else if(!monom.degree) resu.degree=1;  //7x
		else resu.degree=monom.degree;   //7x2
		if(monom.sign=='-') resu.nmber=-resu.nmber; 
		if(monom.children) //Produit... va retourner un polyn�me!
			{resu=[resu];
			 //console.log("children");
			 for(var nch=0;nch<monom.children.length;++nch){
				var child=monom.children[nch];
				//console.log(nch);
				//console.log(child);
				
				var factor=normaPoly(child);
				resu=multiplyPolynom(resu,factor);
			}}
		if(monom.exponent) resu=puissancePoly(resu,monom.exponent);
		console.log(resu);
		//console.log("end monom");
		return resu; //{nmber, degree}
	}
	
/*1 seul repr�sentant par degr�!
resu=[]*/	
function normaPoly(poly){
	var resu={}
    if(!poly.length) return [];
    var dev=[];
    //Tente un d�veloppement, dev est un vrai polyn�me
	poly.forEach(function(monom){
		if(!monom.normalized) monom=normaMonom(monom);  //D�veloppe �ventuellement
		if(Array.isArray(monom)) dev=dev.concat(monom); else dev.push(monom);
	});
	
	dev.forEach(function(monom){
		var degree=monom.degree;
		if(!degree) degree=monom.x?1:0;
		if(!resu[degree]) resu[degree]=monom.nmber;
		else {
			resu[degree]+=monom.nmber; 
			if(!resu[degree]) delete resu[degree];
		}
	});
	var final=[];
	for(var degree in resu){  //On reconstruit un poly normalis�
		final.push({nmber:resu[degree],degree:degree,normalized:true});
	}
	if(poly.exponent) final=puissancePoly(final,poly.exponent); 
	//console.log("End norma poly");
	return final;
}

/*resu: normalis�**/
function multiplyPolynom(pol1,pol2){
	if(!pol1.length) return pol2;
	if(!pol2.length) return pol1;
	var resu=[];
	pol1.forEach(function(m1){
		//console.log(normaMonom(m1));
		pol2.forEach(function(m2){
			var mprod = multiplyMonom(m1,m2); 
			resu.push(mprod);
		});
	});
	//console.log("multiplyPolynom");
	return normaPoly(resu);
}

function addiPolynom(pol1,pol2){  //pol1, pol2 semi-normalis�s ??
	var result=pol1.concat(pol2); 
	return normaPoly(result);
}

/**ndeg>1**/
function puissancePoly(poly,ndeg){
	var resu=poly;
	while(ndeg>1){
		resu=multiplyPolynom(resu,poly);
		--ndeg;
	}
	return resu;
}

/**Polynomes normalis�s**/
function equalPoly(pol1,pol2){
	if(pol1.length!=pol2.length) return false;
	pol1=pol1.sort(cmpDegree);
	pol2=pol2.sort(cmpDegree);
	for(var nm in pol1){
		if(!equalMonom(pol1[nm],pol2[nm])) return false;  //NB n�cessite ordre pour les degr�s
	}
	return true;
}

function cmpDegree(m1,m2){
	return (m1.degree>m2.degree);
}

function equalMonom(m1,m2){
	return(m1.degree==m2.degree && m1.nmber==m2.nmber);
}






